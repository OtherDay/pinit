<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>

    <title>Pin It!</title>

<!-- CSS -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/index.css"/>
    <script type="text/javascript" src="jquery/jqueryproduction-min.js"></script>
</head>

<body>
  <div class="container">
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container-fluid">
<!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="./?page=newsfeed">PIN IT</a>
      </div>


      <form class="navbar-form navbar-left" action="./?page=search" method="GET" id="searchform" role="search">
        <input name="page" value="search" type="hidden" />
        <div class="form-group responsivesearch">
          <input type="text" class="form-control" name="query" placeholder="Search by username...">
        </div>
        <button type="submit"  class="btn btn-default">Search</button>
      </form>

<!-- Collect the nav links, forms, and other content for toggling -->

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
@if (Auth::check())
          <ul class="nav navbar-nav navbar-right"> 
            <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Account Settings<span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="./?page=profile"><?php echo $this->user->data['username']; ?>'s Profile</a></li>
                <li class="divider"></li>
                <li><a href="./?page=logout">Logout</a></li>
              </ul>
            </li>
          </ul>

          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Add <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="./?page=pins&amp;action=add">Add a Pin</a></li>
                <li><a href="./?page=catalogue&amp;action=add">Add a Catalogue</a></li>
              </ul>
            </li>
          </ul>

@else
          <ul class="nav navbar-nav navbar-right">
            <li><a href="./?page=register">Register</a></li>
            <li><a href="./?page=login">Login</a></li>
          </ul>  
@endif

        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </div>
  </div>

@if (Session::has('flash-page-info'))
    <div class="alert alert-info" role="alert"><?php echo $_SESSION['flash']; ?></div>
@endif







@yield('content')

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

<script src="{{ asset('js/respond1.4.2.js')}}"></script>
<script src="{{ asset('js/bootstrap.min.js')}}"></script> 
<script src="{{ asset('js/masonry.js')}}"></script> 
<script src="{{ asset('js/main.js')}}"></script>     

</body>
</html>