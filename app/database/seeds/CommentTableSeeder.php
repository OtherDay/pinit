<?php

class CommentTableSeeder extends Seeder {

	public function run()
	{
		DB::table('comments')->truncate();

		Comment::create(array(
			'user_id' => 1,
			'post_id' => 1,
			'comment' => 'So kitten, much bear, wow 1'
		));
		Comment::create(array(
			'user_id' => 1,
			'post_id' => 2,
			'comment' => 'So kitten, much bear, wow 2'
		));
		Comment::create(array(
			'user_id' => 1,
			'post_id' => 3,
			'comment' => 'So kitten, much bear, wow 3'
		));
		Comment::create(array(
			'user_id' => 2,
			'post_id' => 1,
			'comment' => 'So kitten, much bear, wow 4'
		));
		Comment::create(array(
			'user_id' => 2,
			'post_id' => 2,
			'comment' => 'So kitten, much bear, wow 5'
		));
		Comment::create(array(
			'user_id' => 2,
			'post_id' => 3,
			'comment' => 'So kitten, much bear, wow 6'
		));
		Comment::create(array(
			'user_id' => 3,
			'post_id' => 1,
			'comment' => 'So kitten, much bear, wow 7'
		));
		Comment::create(array(
			'user_id' => 3,
			'post_id' => 2,
			'comment' => 'So kitten, much bear, wow 8'
		));
		Comment::create(array(
			'user_id' => 3,
			'post_id' => 3,
			'comment' => 'So kitten, much bear, wow 9'
		));
	}
}
