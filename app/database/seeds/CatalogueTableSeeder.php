<?php

class CatalogueTableSeeder extends Seeder {

	public function run()
	{
		DB::table('catalogues')->truncate();

		Catalogue::create(array(
			'name'	=>	'Feline Felons',
			'photo'	=>	'http://www.placekitten.com/200/200',
			'user_id'	=>	1
		));
		Catalogue::create(array(
			'name'	=>	'bearly ursine',
			'photo'	=>	'http://www.placebear.com/200/200',
			'user_id'	=>	2
		));
		Catalogue::create(array(
			'name'	=>	'Charlie van Sheen',
			'photo'	=>	'http://www.placesheen.com/200/200',
			'user_id'	=>	1
		));
	}
}
