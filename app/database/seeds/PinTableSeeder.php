<?php

class PinTableSeeder extends Seeder {

	public function run()
	{
		DB::table('pins')->truncate();

		Pin::create(array(
			'catalogue_id' => 1,
			'photo' =>	'http://www.placekitten.com/300/300',
			'description' => 'orrgle barglele burggle barggle',
			));
		Pin::create(array(
			'catalogue_id' => 2,
			'photo' =>	'http://www.placekitten.com/200/300',
			'description' => 'down o the west coast they got a sayin',
			));
		Pin::create(array(
			'catalogue_id' => 2,
			'photo' =>	'http://www.placekitten.com/300/200',
			'description' => 'if youre not drinking then youre not playing',
			));
	}
}
