<?php

class UserTableSeeder extends Seeder {

	public function run()
	{
		DB::table('users')->truncate();

		User::create(array(
			'username'	=>	'KT',
			'name'	=>	'Kate Griffiths',
			'password'	=>	Hash::make('ummm'),
			'email'	=>	'kategriffithsphotography@gmail.com'
		));
		User::create(array(
			'username'	=>	'bob',
			'name'	=>	'bob',
			'password'	=>	Hash::make('bob'),
			'email'	=>	'bob@example.com'
		));
		User::create(array(
			'username'	=>	'otherDay',
			'name'	=>	'Jonas Ermen',
			'password'	=>	Hash::make('123123'),
			'email'	=>	'jonas.ermen@gmail.com'
		));
	}
}
