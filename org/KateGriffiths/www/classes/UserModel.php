<?php

class UserModel extends FormModel
{

	protected $db;
	public $id;
	public $data;

	protected $allowedFields = array( 'id', 'name', 'username', 'password', 'oldpassword', 'password2', 'about', 'location', 'website', 'avatar', 'photo', 'email');

	protected $validation = array(
		'username' => "**REPLACED IN CONSTRUCTOR**",
		'email' => "**REPLACED IN CONSTRUCTOR**",
		'password' => array('Validation::checkRequired', 'Validation::checkPassword'),
		'password2' => "**REPLACED IN CONSTRUCTOR**",
		// 'name' => array('Validation::checkRequired', 'Validation::checkName'),
		// 'about' => array('Validation::checkRequired', 'Validation::checkMessage'),
		// 'website' => array('Validation::checkRequired', 'Validation::checkMessage'),
		// 'avatar' => array('Validation::checkRequired', 'Validation::checkMessage')
	);

	public function __construct($db, $id = 0) {
		$this->validation['password2'] = array(
			'Validation::checkRequired', array($this, 'checkPasswordsMatch')
		);
		$this->validation['username'] = array(
			'Validation::checkRequired', 
			'Validation::checkName',
			array($this, 'checkUsernameUnused')
		);
		$this->validation['email'] = array(
			'Validation::checkRequired',
			'Validation::checkEmail',
			array($this, 'checkEmailUnused')
		);

		$this->db = $db;
		if (is_array($id)) {
			parent::__construct($id);
		} else { // is a number
			$this->processInput();
			$this->id = (int)$id;
			if ($id > 0) {
				$this->load($id);
			}
		}
	}


	public function commit() {
		if ($this->id > 0) {
			$this->update();
		} else {
			$this->create();
		}
	}

	protected function update() {
		// Do a database UPDATE.
		try {
			$statement = $this->db->prepare(
				"UPDATE profile SET name = :name, password = :password, about = :about, location = :location, website = :website, avatar = :avatar, email = :email, photo = :photo WHERE id = :id"
			);
			$statement->bindValue(':name', $this->data['name']);
			$statement->bindValue(':password', $this->data['password']);
			$statement->bindValue(':about', $this->data['about']);
			$statement->bindValue(':location', $this->data['location']);
			$statement->bindValue(':website', $this->data['website']);
			$statement->bindValue(':avatar', $this->data['avatar']);
			$statement->bindValue(':email', $this->data['email']);
			$statement->bindValue(':photo', $this->data['photo']);			
			$statement->bindValue(":id", $this->id, PDO::PARAM_INT);
			$statement->execute();
			$affected_rows = $statement->rowCount();

		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			/// throw ($ex);
			exit;
		}	
	}

	protected function create() {
		$hash = password_hash($this->data['password'], PASSWORD_DEFAULT);
		// Do a database INSERT.
		try {
			$statement = $this->db->prepare(
				"INSERT INTO profile (name, username, password, about, location, website, avatar, email, photo) VALUES (:name, :username, :password, :about, :location, :website, :avatar, :email, :photo)"
			);
			$statement->bindValue(':name', $this->data['name']);
			$statement->bindValue(':username', $this->data['username']);
			$statement->bindValue(':password', $hash);
			$statement->bindValue(':about', $this->data['about']);
			$statement->bindValue(':location', $this->data['location']);
			$statement->bindValue(':website', $this->data['website']);
			$statement->bindValue(':avatar', $this->data['avatar']);
			$statement->bindValue(':email', $this->data['email']);
			$statement->bindValue(':photo', $this->data['photo']);			
			$statement->execute();
			$affected_rows = $statement->rowCount();
			$id = $this->db->lastInsertId();
			$this->id = $id;
			$this->data['id'] = $id;
		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			/// throw ($ex);
			exit();
		}
	}

	public function load($id) {
		$this->id = $id;
		try {
			$statement = $this->db->prepare(
				"SELECT id, name, username, about, location, website, avatar, email, photo FROM profile WHERE id = :id;"
			);
			$statement->bindValue(":id", $id, PDO::PARAM_INT);
			$statement->execute();
			$result = $statement->fetch(PDO::FETCH_ASSOC);
		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			throw ($ex);
		}
		$this->data = $result;
		$this->processPhotoField();
	}

	public function remove() {
		try {
			$statement = $this->db->prepare(
				"DELETE FROM profile WHERE id = :id;"
			);
			$statement->bindValue(":id", $this->id, PDO::PARAM_INT);
			$statement->execute();
			$affected_rows = $statement->rowCount();

		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			throw ($ex);
		}	
	}

	protected function processPhotoField() {
		if (is_string($this->data['avatar'])) {
			if ($this->data['avatar'] !== "") {
				// convert to Image instance
				$img = new Image($this->data['avatar']);
			} else {
				$img = new Image();
			}
			if ($img->error === "") {
				$this->data['avatar'] = $img;
			}
		}
	}


	protected function checkPasswordsMatch($str) {
		// str is password2
		if ($str !== $this->data['password']) {
			return "Passwords need to match.";
		}
		return true;
	}

	protected function checkUsernameUnused($str) {

		try {
			$statement = $this->db->prepare(
				"SELECT COUNT(username) FROM profile WHERE username = :username;"
			);
			$statement->bindValue(":username", $str);
			$statement->execute();
			$usernameCount = $statement->fetchColumn(0); // get value of first column
		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
				exit;
			}
			throw ($ex);
		}
		if ($usernameCount !== 0) {
			return "Username already claimed.";
		}
		return true;
	}

	protected function checkEmailUnused($str) {
		
		try {
			$statement = $this->db->prepare(
				"SELECT COUNT(email) FROM profile WHERE email = :email;"
			);
			$statement->bindValue(":email", $str);
			$statement->execute();
			$usernameCount = $statement->fetchColumn(0); // get value of first column
		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
				exit;
			}
			throw ($ex);
		}
		if ($usernameCount !== 0) {
			return "Email already claimed.";
		}
		return true;
	}

	public function changeEmail($post) {
		
		if (isset($post['email']) && $post['email'] !== "") {
			// check if email address is valid
			$message = Validation::checkEmail($post['email']);
			if ($message !== true) {
				// provide error for the view
				$this->errors['email'] = $message;
				$this->data['email'] = $post['email'];
				return false;
			}

			// check if email address unused
			$message = $this->checkEmailUnused($post['email']);
			if ($message !== true) {
				// provide error for the view
				$this->errors['email'] = $message;
				$this->data['email'] = $post['email'];
				return false;
			}
		}


		// update the database
		try {
			$statement = $this->db->prepare(
				"UPDATE profile SET email = :email, name = :name, about = :about, location = :location, website = :website, avatar = :avatar, photo = :photo WHERE id = :id"
			);
			$statement->bindValue(':email', $post['email']);
			$statement->bindValue(':name', $post['name']);
			$statement->bindValue(':about', $post['about']);
			$statement->bindValue(':location', $post['location']);									
			$statement->bindValue(':website', $post['website']);
			$statement->bindValue(':avatar', $post['avatar']);	
			$statement->bindValue(':photo', $post['photo']);						
			$statement->bindValue(":id", $this->id, PDO::PARAM_INT);
			$statement->execute();

		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
				exit;
			}
			throw ($ex);
		}	
		$this->data['email'] = $post['email'];
		return true;
	}

	public function changePassword($oldpw, $newpw, $newpw2) {
		$error = false;

		// check if old pw is current pw
		$user = self::authenticateUser($this->db, $this->data['username'], $oldpw);
		if ($user->id === 0) {
			// provide error for the view
			$this->errors['oldpassword'] = "Not your current password.";
			$error = true;
		}

		// check if new pw is long enough
		$message = Validation::checkPassword($newpw);
		if ($message !== true) {
			// provide error for the view
			$this->errors['password'] = $message;
			$error = true;
		}

		// check if new pws match
		$this->data['password'] = $newpw;
		$message = $this->checkPasswordsMatch($newpw2);
		if ($message !== true) {
			// provide error for the view
			$this->errors['password2'] = $message;
			$error = true;
		}

		if ($error) { 
			return false;
		}

		// update database
		$hash = password_hash($newpw, PASSWORD_DEFAULT);
		try {
			$statement = $this->db->prepare(
				"UPDATE profile SET password = :password WHERE id = :id"
			);
			$statement->bindValue(':password', $hash);
			$statement->bindValue(":id", $this->id, PDO::PARAM_INT);
			$statement->execute();

		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
				exit;
			}
			throw ($ex);
		}	

		return true;
	} 

	static public function fetchAll($db) {
		$collection = array();

		// get all of the things from the database.
		try {
			$statement = $db->prepare(
				"SELECT * FROM profile" //changed from team
			);
			$statement->execute();
			
			// get the catalogue record by record
			while ($record = $statement->fetch(PDO::FETCH_ASSOC)) {
				$obj = new self($db);
				$obj->processInput($record);
				$obj->processPhotoField();
				array_push($collection, $obj);
			}

		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			throw ($ex);
		}

		return $collection;
	}

	static public function fetchAllBySearch($db, $query) {
		$collection = array();

		// get all of the things from the database.
		try {
			$statement = $db->prepare(
				"SELECT id, name, username, avatar FROM profile WHERE username LIKE CONCAT('%',:query1,'%') OR username LIKE CONCAT('%',:query2,'%')"
			);
			$statement->bindValue(":query1", $query);
			$statement->bindValue(":query2", $query);
			$statement->execute();
			
			// get the catalogue record by record
			while ($record = $statement->fetch(PDO::FETCH_ASSOC)) {
				$obj = new self($db);
				$obj->processInput($record);
				$obj->processPhotoField();
				array_push($collection, $obj);
			}

		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			throw ($ex);
		}

		return $collection;
	}

	static public function authenticateUser($db, $un, $pw) {
		// find the user with this username
		$user = UserModel::findUsername($db, $un);
		if (!$user) {
			// not a real user
			$user = new UserModel($db);
			return $user;
		}
		// validate passwords
		if (password_verify($pw, $user->data['password'])) {
			// passwords match
			if (password_needs_rehash($user->data['password'], PASSWORD_DEFAULT)) {
				// do database update here.
				$user->data['password'] = password_hash($pw, PASSWORD_DEFAULT);
				$user->commit();
			}
			return $user;
		}

		// no user found, pass back empty user
		return new UserModel($db);
	}

	static public function findUsername($db, $un) {
		try {
			$statement = $db->prepare(
				"SELECT id, username, password FROM profile WHERE username = :username"
			);
			$statement->bindValue(":username", $un);
			$statement->execute();
			$result = $statement->fetch(PDO::FETCH_ASSOC);
		} catch (PDOException $ex) {
			if (HOST_TYPE === "dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
				exit();
			}
			throw($ex);
		}
		if ($result) {
			$user = new UserModel($db);
			$user->data = $result;
			$user->id = $user->data['id'];
			return $user;
		}
		return false;
	}
}






