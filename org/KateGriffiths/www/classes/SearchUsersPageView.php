

<?php

class SearchUsersPageView extends PageView
{
	public $slug = "searchusers";
	protected $collection;

	function __construct($collection, $query) {
		// $this->model = $model;
		$this->collection = $collection;
		parent::__construct($this->slug);
	}	

	function page_title () {
		echo "Search Results";
	}

}