<?php

class SinglePinPageView extends PageView
{
	public $slug = "singlepin";
	protected $pins;

	function __construct($model) {
		$this->model = $model;
		parent::__construct($this->slug);
	}	

	function page_title () {
		echo "Pin Page View";
	}

}