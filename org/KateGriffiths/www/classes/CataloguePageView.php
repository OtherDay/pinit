
<?php

class CataloguePageView extends PageView
{
	public $slug = "catalogue";
	protected $catalogues;
	protected $pins;

	function __construct($model, $pins) {
		$this->model = $model;
		$this->pins = $pins;
		parent::__construct($this->slug);
	}	

	function page_title () {
		echo "Your Catalogue";
	}

}