<?php

class UserIdPageView extends PageView
{
	public $slug = "userid";
	protected $pins;

	function __construct($pins) {
		$this->pins = $pins;
		parent::__construct($this->slug);
	}	

	function page_title () {
		echo "Pin View"; 
	}

}