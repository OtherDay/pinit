<?php
/* DONE */
abstract class FormModel extends BaseModel
{
	public $data;
	protected $input;

	public $debugOutput;

	public $errors = array();

	// protected $allowedFields = array();

	public function __construct($input) {
		$this->input = $input;
		$this->processInput();
		if (count($input) > 0) {
			if ($this->validateData()) {
				$this->commit();
			}
		}
	}

	abstract protected function commit();

	public function validateData() {
		$errorFound = false;
		// all fields are required.
		// if field is blank, record error.
		$this->errors = array();
		foreach ($this->validation as $fieldName => $validationMethods) {
			$this->errors[$fieldName] = "";

			foreach($validationMethods as $method) {
				if (!is_callable($method)) {
					throw new Exception("found uncallable validation method on " . $fieldName);
				}
				$message = call_user_func($method, $this->data[$fieldName]);
				if ($message !== true) {
					$this->errors[$fieldName] = $message;
					$errorFound = true;
				}
			} // end validationMethods foreach
		} // end fieldName foreach

		// return true if no errors, false if errors.
		return !$errorFound;
	}

}