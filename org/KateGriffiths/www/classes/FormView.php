<?php

abstract class FormView extends PageView
{

	protected $data = array();
	protected $model = array();
	protected $errors = array();

	// override PageView's constructor...
	function __construct ($slug, $model) {
		$this->model = $model;
		$this->data = $model->data;
		$this->errors = $model->errors;

		// ...but invoke PageView's constructor anyhow.
		parent::__construct($slug);
	}

	abstract public function page_title ();

	protected function showSelectOptions($options, $selectedValue) {
		foreach ($options as $option) {
			echo '<option value="' . $option->id. '"';
			if ($option->id === $selectedValue) {
				echo ' selected="selected"';
			}
			echo '>' . $option->data['name'] . '</option>';
		}
	}

	protected function echoIfError($fieldName, $str){
		if ($this->errors[$fieldName] !== "") {
			echo $str;
		}
	}

	protected function showError($fieldName) {
		if ($this->errors[$fieldName] !== "") {
			?>
		    <span class="form-control-feedback"></span>
            <span class="help-block"><?php echo $this->errors[$fieldName]; ?></span>
			<?php
		}
	}

	public function page_contents() {
		if (HOST_TYPE === "dev" && isset($this->model->debugOutput)) {
			?>
			<pre><?php echo $this->model->debugOutput; ?></pre>
			<?php
		}
		parent::page_contents();
	}

}




