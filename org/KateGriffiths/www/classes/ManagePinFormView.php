<?php

class ManagePinFormView extends FormView
{
	public $slug = "managepins";
	protected $catalogues;

	public function __construct ($model, $catalogues) {
		parent::__construct($this->slug, $model);
		$this->catalogues = $catalogues;
	}

	public function page_title () {
		echo "Create a pin";
	}
}