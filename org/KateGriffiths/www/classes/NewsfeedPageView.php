<?php

class NewsfeedPageView extends PageView
{
	public $slug = "newsfeed";
	protected $newsfeed;

	function __construct($result) {
		$this->newsfeed = $result['collection'];
		
		$limit = $result['limit'];
		$offset = $result['offset'];

		$this->page = ($offset / $limit) + 1;
		$this->pageCount = (int) ceil($result['recordCount'] / $limit); 

		parent::__construct($this->slug);
	}	

	function page_title () {
		echo $this->user->data['name'] . " Newsfeed";
	}

}