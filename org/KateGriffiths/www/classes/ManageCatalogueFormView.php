


<?php

	class ManageCatalogueFormView extends FormView
	{
		public $slug = "managecatalogue";
		protected $catalogue;

		public function __construct ($model, $catalogue) {
			parent::__construct($this->slug, $model);
			$this->catalogue = $catalogue;
		}

		public function page_title () {
			echo "Create a Catalogue";
		}
}