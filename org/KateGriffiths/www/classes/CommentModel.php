<?php

class CommentModel extends FormModel
{
	protected $db;
	public $id;
	public $data;

	public $user;

	protected $allowedFields = array( 'id', 'userid', 'postid', 'comment', 'datecreated');

	protected $validation = array(
		'userid' => array('Validation::checkRequired'),
		'postid' => array('Validation::checkRequired'),
		//'comment' => array()
	);


	public function __construct($db, $id = 0) {
		$this->db = $db;
		if (is_array($id)) {
			parent::__construct($id);
		} else { // is a number
			$this->processInput();
			$this->id = (int)$id;
			if ($id > 0) {
				$this->load($id);
			}
		}
	}

	public function commit() {
		if ($this->id > 0) {
			$this->update();
		} else {
			$this->create();
		}
	}

	protected function update() {
		// Do a database UPDATE.
		try {
			$statement = $this->db->prepare(
				"UPDATE comment SET comment = :comment WHERE id = :id;"
			);
			$statement->bindValue(':comment', $this->data['comment']);
			$statement->bindValue(":id", $this->id, PDO::PARAM_INT);
			$statement->execute();
			$affected_rows = $statement->rowCount();

		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			/// throw ($ex);
			exit;
		}	
	}

	protected function create() {
		// Do a database INSERT.
		try {
			$statement = $this->db->prepare(
				"INSERT INTO comment (comment, userid, postid) VALUES (:comment, :userid, :postid)"
			);
			$statement->bindValue(':comment', $this->data['comment']);
			$statement->bindValue(':userid', $this->data['userid']);
			$statement->bindValue(':postid', $this->data['postid']);
			$statement->execute();
			$affected_rows = $statement->rowCount();
			$id = $this->db->lastInsertId();
			$this->id = $id;
			$this->data['id'] = $id;
		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			/// throw ($ex);
			exit();
		}
	}


	public function load($id) {
		$this->id = $id;
		try {
			$statement = $this->db->prepare("SELECT id, userid, comment FROM comment WHERE id = :id;");
			$statement->bindValue(":id", $id, PDO::PARAM_INT);
			$statement->execute();
			$result = $statement->fetch(PDO::FETCH_ASSOC);
		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			throw ($ex);
		}
		$this->data = $result;
		$this->user = new UserModel($this->db, $this->data['userid']);
	}

	public function remove() {
		try {
			$statement = $this->db->prepare(
				"DELETE FROM comment WHERE id = :id;"
			);
			$statement->bindValue(":id", $this->id, PDO::PARAM_INT);
			$statement->execute();
			$affected_rows = $statement->rowCount();

		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			throw ($ex);
		}	
	}

	static public function fetchAll($db) {
		$collection = array();

		// get all of the things from the database.
		try {
			// heredoc syntax looks ugly but helps syntax highlighting
			$sql = <<<SQL
				SELECT c.id, 
				       c.comment, 
				       COUNT(p.comment) AS pincount 
				FROM   comment AS c 
				       LEFT OUTER JOIN post AS p 
				              ON c.id = p.comment 
				GROUP BY c.id;			
SQL;
			$statement = $db->prepare( $sql );
			$statement->execute();
			
			// get the products record by record
			while ($record = $statement->fetch(PDO::FETCH_ASSOC)) {
				$obj = new self($db);
				$obj->processInput($record);
				$obj->user = new UserModel($db, $record['userid']);
				array_push($collection, $obj);
			}

		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			throw ($ex);
		}

		return $collection;
	}

	static public function fetchAllByUser($db, $user_id) {
		$collection = array();

		// get all of the things from the database.
		try {
			// heredoc syntax looks ugly but helps syntax highlighting
			$sql = <<<SQL
				SELECT c.id, 
				       c.comment, 
				       COUNT(p.commentid) AS pincount 
				FROM   comment AS c 
				       LEFT OUTER JOIN post AS p 
				              ON c.id = p.commentid 
				WHERE c.userid = :userid
				GROUP BY c.id;			
SQL;
			$statement = $db->prepare( $sql );
			$statement->bindValue(':userid', $user_id);
			$statement->execute();
			
			// get the products record by record
			while ($record = $statement->fetch(PDO::FETCH_ASSOC)) {
				$obj = new self($db);
				$obj->processInput($record);
				$obj->user = new UserModel($db, $record['userid']);
				array_push($collection, $obj);
			}

		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			throw ($ex);
		}

		return $collection;
	}



	static public function fetchAllByPin($db, $pin_id) {
		$collection = array();

		// get all of the things from the database.
		try {
			// heredoc syntax looks ugly but helps syntax highlighting
			$sql = <<<SQL
				SELECT c.id, 
				       c.comment,
				       c.userid,
				       c.postid,
				       c.datecreated
				FROM   comment AS c 
				WHERE c.postid = :pinid
				GROUP BY c.id;			
SQL;
			$statement = $db->prepare( $sql );
			$statement->bindValue(':pinid', $pin_id);
			$statement->execute();
			
			// get the products record by record
			while ($record = $statement->fetch(PDO::FETCH_ASSOC)) {
				$obj = new self($db);
				$obj->processInput($record);
				$obj->user = new UserModel($db, $record['userid']);
				array_push($collection, $obj);
			}

		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			throw ($ex);
		}

		return $collection;
	}

	static public function checkValidCatalogue($db, $id) {
		if ($id <= 0) {
			return "Catalogue must be positive.";
		}
		$comment = new self($db, $id);
		if ($comment->id === 0) {
			return "Catalogue does not exist.";
		}
		return true;

	}

}