<?php

class CataloguesPageView extends PageView
{
	public $slug = "catalogues";
	protected $model; 
	protected $data;


	function __construct($model) {
		$this->model = $model;
		$this->data = $this->model->data;
		$this->catalogues = $this->model->getProducts();
		parent::__construct($this->slug);
	}	

	function page_title () {
		echo $this->model->data['name'];
	}

}