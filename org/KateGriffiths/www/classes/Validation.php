<?php

class Validation
{

	static public function checkRequired($str) {
		if (!isset($str) || $str === "" ) {
			return "This field is required.";
		}
		return true;
	}

	static public function checkName($str) {
		if (strlen($str) == 1) {
			return "Name too short.";
		}
		return true;
	}

	static public function checkMessage($str) {
		if (strlen($str) !== 0 && strlen($str) < 5) {
			return "Message too short.";
		}
		return true;
	}

	static public function checkEmail($str) {
		if (!filter_var($str, FILTER_VALIDATE_EMAIL)) {
			return "Enter a valid email address.";
		}
		return true;
	}

	static public function checkNumeric($str) {
		if (!filter_var($str, FILTER_VALIDATE_FLOAT)) {
			return "Must be a valid number with decimal places.";
		}
		return true;
	}

	static public function checkPassword($str) {
		if (strlen($str) < 6) {
			return "Must be at least 6 characters long.";
		}
		// SOMEDAY: Add extra password restrictions, 
		// such as capitals, numbers, symbols, oh my!
		
		return true;
	}

}