<?php

class CatalogueModel extends FormModel
{
	protected $db;
	public $id;
	public $data;

	public $user;

	protected $allowedFields = array( 'id', 'name', 'photo', 'userid' );

	protected $validation = array(
		'name' => array('Validation::checkRequired', 'Validation::checkName'),
	);


	public function __construct($db, $id = 0) {
		$this->db = $db;
		if (is_array($id)) {
			parent::__construct($id);
		} else { // is a number
			$this->processInput();
			$this->id = (int)$id;
			if ($id > 0) {
				$this->load($id);
			}
		}
	}

	public function commit() {
		if ($this->id > 0) {
			$this->update();
		} else {
			$this->create();
		}
	}

	protected function update() {
		// Do a database UPDATE.
		try {
			$statement = $this->db->prepare(
				"UPDATE catalogue SET name = :name, photo = :photo WHERE id = :id;"
			);
			$statement->bindValue(':name', $this->data['name']);
			$statement->bindValue(':photo', $this->data['photo']->filename);
			$statement->bindValue(":id", $this->id, PDO::PARAM_INT);
			$statement->execute();
			$affected_rows = $statement->rowCount();

		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			/// throw ($ex);
			exit;
		}	
	}

	protected function create() {
		// Do a database INSERT.
		try {
			$statement = $this->db->prepare(
				"INSERT INTO catalogue (userid, photo, name) VALUES (:userid, :photo, :name)"
			);
			$statement->bindValue(':userid', $this->data['userid']);
			$statement->bindValue(':photo', $this->data['photo']->filename);
			$statement->bindValue(':name', $this->data['name']);
			$statement->execute();
			$affected_rows = $statement->rowCount();
			$id = $this->db->lastInsertId();
			$this->id = $id;
			$this->data['id'] = $id;
		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			/// throw ($ex);
			exit();
		}
	}


	public function load($id) {
		$this->id = $id;
		try {
			$statement = $this->db->prepare("SELECT id, userid, photo, name FROM catalogue WHERE id = :id;");
			$statement->bindValue(":id", $id, PDO::PARAM_INT);
			$statement->execute();
			$result = $statement->fetch(PDO::FETCH_ASSOC);
		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			throw ($ex);
		}
		$this->data = $result;
		$this->processPhotoField();
		$this->user = new UserModel($this->db, $this->data['userid']);
	}

	public function remove() {
		try {
			$statement = $this->db->prepare(
				"DELETE FROM catalogue WHERE id = :id;"
			);
			$statement->bindValue(":id", $this->id, PDO::PARAM_INT);
			$statement->execute();
			$affected_rows = $statement->rowCount();

		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			throw ($ex);
		}	
	}

	public function getPins () {
		return PinsModel::fetchAllByCatalogue($this->db, $this->id);
	}


	static public function fetchAll($db) {
		$collection = array();

		// get all of the things from the database.
		try {
			// heredoc syntax looks ugly but helps syntax highlighting
			$sql = <<<SQL
				SELECT c.id, 
				       c.name, 
				       COUNT(p.catalogueid) AS pincount 
				FROM   catalogue AS c 
				       LEFT OUTER JOIN post AS p 
				              ON c.id = p.catalogueid 
				GROUP BY c.id;			
SQL;
			$statement = $db->prepare( $sql );
			$statement->execute();
			
			// get the products record by record
			while ($record = $statement->fetch(PDO::FETCH_ASSOC)) {
				$obj = new self($db);
				$obj->processInput($record);
				$obj->user = new UserModel($db, $record);//['userid']
				array_push($collection, $obj);
			}

		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			throw ($ex);
		}

		return $collection;
	}

	static public function fetchLimit($db, $limit, $offset) {
		$result = array(
			'collection' => array(),
			'recordCount' => 0,
			'limit' => $limit,
			'offset' => $offset,
		);

		try {

			// get recordcount

			$statement = $db->prepare(
				"SELECT COUNT(id) FROM catalogue;"
			);
			$statement->execute();
			$result['recordCount'] = (int) $statement->fetchColumn();

			// get records

			$statement = $db->prepare(
				"SELECT * FROM catalogue LIMIT :limit OFFSET :offset;"
			);
			$statement->bindValue(":limit", $limit, PDO::PARAM_INT);
			$statement->bindValue(":offset", $offset, PDO::PARAM_INT);
			$statement->execute();

			// get the post record by record
			while ($record = $statement->fetch(PDO::FETCH_ASSOC)) {
				$obj = new self($db);
				$obj->processInput($record);
				$obj->processPhotoField();
				$obj->catalogue = new CatalogueModel($db, $record['userid']);
				// $obj->comments = CommentModel::fetchAllByPin($db, $obj->id);
				array_push($result['collection'], $obj);
			}

		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			throw ($ex);
		}

		return $result;
	}





	static public function fetchAllByUser($db, $user_id) {
		$collection = array();

		// get all of the things from the database.
		try {
			// heredoc syntax looks ugly but helps syntax highlighting
			$sql = <<<SQL
				SELECT c.id, 
				       c.name, 
				       COUNT(p.catalogueid) AS pincount 
				FROM   catalogue AS c 
				       LEFT OUTER JOIN post AS p 
				              ON c.id = p.catalogueid 
				WHERE c.userid = :userid
				GROUP BY c.id;			
SQL;
			$statement = $db->prepare( $sql );
			$statement->bindValue(':userid', $user_id);
			$statement->execute();
			
			// get the products record by record
			while ($record = $statement->fetch(PDO::FETCH_ASSOC)) {
				$obj = new self($db);
				$obj->processInput($record);
				$obj->user = new UserModel($db, $record);
				array_push($collection, $obj);
			}

		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			throw ($ex);
		}

		return $collection;
	}

		protected function processPhotoField() {
		if (is_string($this->data['photo'])) {
			if ($this->data['photo'] !== "") {
				// convert to Image instance
				$img = new Image($this->data['photo']);
			} else {
				$img = new Image();
			}
			if ($img->error === "") {
				$this->data['photo'] = $img;
			}
		}
	}

	static public function checkValidCatalogue($db, $id) {
		if ($id <= 0) {
			return "Catalogue must be positive.";
		}
		$catalogue = new self($db, $id);
		if ($catalogue->id === 0) {
			return "Catalogue does not exist.";
		}
		return true;

	}

}