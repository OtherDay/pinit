<?php

class ManageUserProfileFormView extends FormView
{
	public $slug = "manageuserprofile";
	public $profile;

	public function __construct($model) {
		parent::__construct($this->slug, $model);
	}

	public function page_title() {
		echo $this->model->data['name'] . " Profile";
	}

}