<?php

class ManageNewsfeedFormView extends FormView
{
	public $slug = "managenewsfeed";
	protected $categories;

	public function __construct($model, $categories) {
		$this->categories = $categories;
		parent::__construct($this->slug, $model);
	}

	public function page_title() {
		echo "Newsfeed";
	}

}