<?php

class PinsModel extends FormModel
{

	protected $db;
	public $id;
	public $data;

	public $catalogue;

	protected $allowedFields = array( 'id', 'catalogueid', 'description', 'datecreated', 'photo');

	protected $validation = array(
		'photo' => array('PinsModel::checkPhotoIsImageObject'),
		'description' => array()
	);

	public function __construct($db, $id = 0) {
		$this->validation['catalogueid'] = array( array($this, 'checkCatalogueExists') );

		$this->db = $db;
		if (is_array($id)) {
			parent::__construct($id);
		} else { // is a number
			$this->processInput();
			$this->id = (int)$id;
			if ($id > 0) {
				$this->load($id);
			} else {
				$this->data['photo'] = new Image();
			}
		}
	}

	public function commit() {
		if ($this->id > 0) {
			$this->update();
		} else {
			$this->create();
		}
	}

	protected function update() {
		// Do a database UPDATE.
		try {
			$statement = $this->db->prepare(
				"UPDATE description FROM post SET description = :description WHERE id = :id;"
			);
			$statement->bindValue(':description', $this->data['description']);
			$statement->bindValue(":id", $this->id, PDO::PARAM_INT);
			$statement->execute();
			$affected_rows = $statement->rowCount();


		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			/// throw ($ex);
			exit;
		}	
	}

	protected function create() {
		// Do a database INSERT.
		try {
			$statement = $this->db->prepare(
				"INSERT INTO post (catalogueid, photo, description, datecreated) VALUES (:catalogueid, :photo, :description, NOW() )"
			);
			
			$statement->bindValue(':catalogueid', $this->data['catalogueid']);
			$statement->bindValue(':photo', $this->data['photo']->filename);
			$statement->bindValue(':description', $this->data['description']); 
			//$statement->bindValue(':photo', basename($this->data['photo']->filename));
			$statement->execute();
			$affected_rows = $statement->rowCount();
			$id = $this->db->lastInsertId();
			$this->id = $id;
			$this->data['id'] = $id;

		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			/// throw ($ex);
			exit();
		}
	}

	public function load($id) {
		$this->id = $id;
		try {
			$statement = $this->db->prepare(
				"SELECT * FROM post WHERE id = :id"
			);
			$statement->bindValue(":id", $id, PDO::PARAM_INT);
			$statement->execute();
			$result = $statement->fetch(PDO::FETCH_ASSOC);
		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			throw ($ex);
		}
		$this->data = $result;
		$this->processPhotoField();
		$this->catalogue = new CatalogueModel($this->db, $this->data['catalogueid']);
		$this->comments = CommentModel::fetchAllByPin($this->db, $this->id);
	}

	protected function processPhotoField() {
		if (is_string($this->data['photo'])) {
			if ($this->data['photo'] !== "") {
				// convert to Image instance
				$img = new Image($this->data['photo']);
			} else {
				$img = new Image();
			}
			if ($img->error === "") {
				$this->data['photo'] = $img;
			}
		}
	}

	public function remove() {
		try {
			$statement = $this->db->prepare(
				"DELETE FROM post WHERE id = :id;"
			);
			$statement->bindValue(":id", $this->id, PDO::PARAM_INT);
			$statement->execute();
			$affected_rows = $statement->rowCount();

		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			throw ($ex);
		}	
	}


	static public function fetchAll($db) {
		$collection = array();

		// get all of the things from the database.
		try {
			$statement = $db->prepare(
				"SELECT * FROM post"
			);
			$statement->execute();
			
			// get the catalogue members record by record
			while ($record = $statement->fetch(PDO::FETCH_ASSOC)) {
				$obj = new self($db);
				$obj->processInput($record);
				$obj->processPhotoField();
				$obj->catalogue = new CatalogueModel($db, $record['catalogueid']);
				array_push($collection, $obj);
			}

		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			throw ($ex);
		}

		return $collection;
	}


	// static public function fetchAllByPinId($db, $singlepin_id) {
	// 	$collection = array();

	// 	// get all of the things from the database.
	// 	try {
	// 		$statement = $db->prepare(
	// 			"SELECT * FROM post WHERE id = :id"
	// 		);
	// 		$statement->bindValue(':id' , $id);
	// 		$statement->execute();
			
	// 		// get the catalogue members record by record
	// 		while ($record = $statement->fetch(PDO::FETCH_ASSOC)) {
	// 			$obj = new self($db);
	// 			$obj->processInput($record);
	// 			$obj->processPhotoField();
	// 			$obj->catalogue = new CatalogueModel($db, $record['catalogueid']);
	// 			array_push($collection, $obj);
	// 		}

	// 	} catch (PDOException $ex) {
	// 		if (HOST_TYPE ==="dev") {
	// 			echo "<pre>"; print_r($ex); echo "</pre>";
	// 		}
	// 		throw ($ex);
	// 	}

	// 	return $collection;
	// }




	static public function fetchAllByUserId($db, $user_id) {
		$collection = array();

		// get all of the things from the database.
		try {
			$statement = $db->prepare(
				"SELECT post.*, catalogue.userid FROM post LEFT JOIN catalogue ON post.catalogueid = catalogue.id WHERE userid = :id"
			);
			$statement->bindValue(':id' , $user_id);
			$statement->execute();
			
			// get the catalogue members record by record
			while ($record = $statement->fetch(PDO::FETCH_ASSOC)) {
				$obj = new self($db);
				$obj->processInput($record);
				$obj->processPhotoField();
				$obj->catalogue = new CatalogueModel($db, $record['catalogueid']);
				$obj->user = new UserModel($db, $obj->catalogue->data['userid']);
				$obj->comments = CommentModel::fetchAllByPin($db, $obj->id);				
				array_push($collection, $obj);
			}

		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			throw ($ex);
		}

		return $collection;
	}

	static public function fetchAllByCatalogue($db, $catalogueid) {
		$collection = array();

		// get all of the things from the database.
		try {
			$statement = $db->prepare(
				"SELECT * FROM post WHERE catalogueid = :catalogueid"
			);
			$statement->bindValue(':catalogueid', $catalogueid);
			$statement->execute();
			
			// get the catalogue members record by record
			while ($record = $statement->fetch(PDO::FETCH_ASSOC)) {
				$obj = new self($db);
				$obj->processInput($record);
				$obj->processPhotoField();
				$obj->catalogue = new CatalogueModel($db, $record['catalogueid']);
				$obj->comments = CommentModel::fetchAllByPin($db, $obj->id);
				array_push($collection, $obj);
			}

		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			throw ($ex);
		}

		return $collection;
	}



	static public function fetchLimit($db, $limit, $offset) {
		$result = array(
			'collection' => array(),
			'recordCount' => 0,
			'limit' => $limit,
			'offset' => $offset,
		);

		try {

			// get recordcount

			$statement = $db->prepare(
				"SELECT COUNT(id) FROM post;"
			);
			$statement->execute();
			$result['recordCount'] = (int) $statement->fetchColumn();

			// get records

			$statement = $db->prepare(
				"SELECT * FROM post LIMIT :limit OFFSET :offset;"
			);
			$statement->bindValue(":limit", $limit, PDO::PARAM_INT);
			$statement->bindValue(":offset", $offset, PDO::PARAM_INT);
			$statement->execute();

			// get the post record by record
			while ($record = $statement->fetch(PDO::FETCH_ASSOC)) {
				$obj = new self($db);
				$obj->processInput($record);
				$obj->processPhotoField();
				$obj->catalogue = new CatalogueModel($db, $record['catalogueid']);
				$obj->comments = CommentModel::fetchAllByPin($db, $obj->id);
				array_push($result['collection'], $obj);
			}

		} catch (PDOException $ex) {
			if (HOST_TYPE ==="dev") {
				echo "<pre>"; print_r($ex); echo "</pre>";
			}
			throw ($ex);
		}

		return $result;
	}


	static protected function checkPhotoIsImageObject($value) {
		// TODO
		// return true if value is an image and has no error!
		// else return the error or "not an image object"
		if (get_class($value) !== "Image"  || $value->filename == "") {
			return "Photo not uploaded.";
		}
		if ($value->filename !== "" && $value->error !== "") {
			return $value->error;
		}
		if (is_uploaded_file($value->filename)) {
			return "Photo not uploaded.";
		}
		return true;
	}

	public function checkCatalogueExists($value) {
		return CatalogueModel::checkValidCatalogue($this->db, $value);
	}

}