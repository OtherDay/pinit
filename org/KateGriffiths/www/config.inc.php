<?php

if ($_SERVER['HTTP_HOST'] === "localhost") {
	define("HOST_TYPE", "dev");
} else {
	define("HOST_TYPE", "live");
}

switch (HOST_TYPE) {
	case "dev":
		error_reporting(E_ALL);
		define("DB_DSN", 'mysql:host=localhost;dbname=pinit;charset=utf8');
		define("DB_USERNAME", 'root');
		define("DB_PASSWORD", '');
		break;
	case "live": // no break
	default:
		error_reporting(1);
		ini_set('display_errors', false);
		ini_set("log_errors", 1);
		ini_set("error_log", "./php-error.log");
		define("DB_DSN", 'mysql:host=localhost;dbname=pinit;charset=utf8');
		define("DB_USERNAME", 'kate');
		define("DB_PASSWORD", 'tester');
	// 
}