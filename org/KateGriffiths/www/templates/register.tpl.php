                                                <!-- DONE -->

    <form class="form-horizontal" role="form" enctype="multipart/form-data" method="POST" action="./?page=register">
        <div class="container-login">      
            <div class="form-group <?php $this->echoIfError('username', "has-error has-feedback"); ?>">
                <label for="username" class="col-sm-2 control-label">Username</label>
                <div class="col-xs-4">
                    <input id="username" name="username" class="form-control"
                    value="<?php $this->ee($this->data['username']); ?>"/>
                </div>    
                <?php $this->showError('username'); ?>
            </div>

            <div class="form-group <?php $this->echoIfError('name', "has-error has-feedback"); ?>">
                <label for="name" class="col-sm-2 control-label">Name</label>
                <div class="col-xs-4">
                    <input id="name" name="name" class="form-control"
                    value="<?php $this->ee($this->data['name']); ?>"/>
                </div>    
                <?php $this->showError('name'); ?>
            </div>            

        <div class="form-group <?php $this->echoIfError('email', "has-error has-feedback"); ?>">
            <label for="email" class="col-sm-2 control-label">Email Address</label>
            <div class="col-xs-4">
                <input id="email" name="email" type="email" class="form-control"
                value="<?php $this->ee($this->data['email']); ?>" />
            </div>    
            <?php $this->showError('email'); ?>
        </div>

        <div class="form-group <?php $this->echoIfError('password', "has-error has-feedback"); ?>">
            <label for="password" class="col-sm-2 control-label">Password</label>
            <div class="col-xs-4">
                <input id="password" name="password" type="password" class="form-control"
                value="<?php $this->ee($this->data['password']); ?>" />
            </div>    
            <?php $this->showError('password'); ?>
        </div>

        <div class="form-group <?php $this->echoIfError('password2', "has-error has-feedback"); ?>">
            <label for="password2" class="col-sm-2 control-label">Confirm Password</label>
            <div class="col-xs-4">
            <input id="password2" name="password2" type="password" class="form-control"
                value="<?php $this->ee($this->data['password2']); ?>" />
            </div>    
            <?php $this->showError('password2'); ?>
        </div>

        
        <div class="form-group btn btn-block right cf ">
            <input type="hidden" name="action" value="add" />
            <button class="btn btn-primary action-btn-3">
                <span></span> Sign Up
            </button>
        </div>
        </div>    
    </form>