


<div class="container searchcontainer">
  <h2>SEARCH RESULTS</h2>
<?php $column_count = 1; ?>
<?php foreach($this->collection as $result): ?>

  <?php if ($column_count === 1): ?>

  <div class="row">
  <?php endif; ?>

          <div class="cf col-sm-12 cols">
            <div>
                <ul>  
        		      <li><h4><a href="?page=userid&amp;id=<?php $this->ee($result->data['id']); ?>"><?php $this->ee($result->data['name']); ?></a></h4></li>
        	      </ul>	  
            </div>     
      	 </div>

<?php if ($column_count === 5): ?>

  </div><!-- end row -->
<?php endif; ?>

<?php
  $column_count += 1;
  if ($column_count > 4) { 
    $column_count = 1;
  }
?>
<?php endforeach; ?> 
</div>
