<div class="container singlepincontainer">
	  <img class="main-img" src="./<?php $this->ee($this->model->data['photo']->filename); ?>" alt=""/>

		<div class="pin_owner_comment_lg">
			<p><?php $this->ee($this->model->data['description']); ?></p> 
		</div>
		<div class="pin_owner_lg">
  			<img src="<?php echo $this->get_gravatar($this->model->catalogue->user->data['email']); ?>" />
			<ul>
  				<li><?php $this->ee($this->model->catalogue->user->data['name']); ?></li> 
				<a href="./?page=catalogue&amp;id=<?php $this->ee($this->model->id); ?>"><li><?php $this->ee($this->model->catalogue->data['name']); ?></li></a> 
  			</ul> 
		</div>
	<?php foreach ($this->model->comments as $comment): ?>
		<div class="pin-user-comment_lg">
		<img src="<?php echo $this->get_gravatar($comment->user->data['email']); ?>" />
			<ul>
				<a href="./?page=userid&amp;id=<?php $this->ee($comment->user->id); ?>"><li><?php $this->ee($comment->user->data['name']); ?></li></a>
				<li><p><?php $this->ee($comment->data['comment']); ?></p></li>
			</ul> 
		</div>
	<?php endforeach; ?>

		<div class="pin-user-commentbox-add_lg">
			<form action="./?page=comment" method="POST">
				<textarea class="comment_add_modal_lg" name="comment" placeholder="Add a comment..."></textarea>
				<input type="hidden" name="action" value="add" />
				<input type="hidden" name="postid" value="<?php echo $this->model->id; ?>" />
				<button type="submit" class="btn btn-default btn-block navbar-btn navbar-right comment_add_submit_lg">Submit</button>
			</form>
		</div>
</div>	
    		
	