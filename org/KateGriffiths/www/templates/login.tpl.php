    <form class="form-horizontal" role="form" enctype="multipart/form-data" method="POST" action="./?page=login">
    <div class="container-login">    
        <div class="form-group <?php $this->echoIfError('username', "has-error has-feedback"); ?>">
            <label for="username" class="col-sm-2 control-label">Username</label>
            <div class="col-xs-4">
                <input id="username" name="username" class="form-control"
                value="<?php $this->ee($this->data['username']); ?>"/>
            </div>    
            <?php $this->showError('username'); ?>
        </div>

        <div class="form-group <?php $this->echoIfError('password', "has-error has-feedback"); ?>">
            <label for="password" class="col-sm-2 control-label">Password</label>
            <div class="col-xs-4">
                <input id="password" name="password" type="password" class="form-control"
                value="<?php $this->ee($this->data['password']); ?>" />
                <?php $this->showError('password'); ?>
            </div>    
            
        </div>
        
        <div class="form-group btn btn-block right cf ">
            <button class="btn btn-primary action-btn-2">
                <span></span> Log In
            </button>
        </div>
    </div>    
    </form>
