


 <form class="form-horizontal" role="form" enctype="multipart/form-data" method="POST" action="?page=pins">
    <div class="container-login">      

     <!--  <pre><?php //var_dump($this->model->errors); ?></pre> -->
      <div class="form-group <?php $this->echoIfError('photo', "has-error has-feedback"); ?>">
        <label for="photo" class="col-sm-2 control-label">Upload a photo</label>
        <div class="col-xs-4">
          <input id="photo" name="photo" class="form-control" type="file" />
        </div>
      </div> 
      <div>  
        <p class="help-block">File must be a JPG, PNG or GIF smaller than 
              <?php echo min( ini_get( 'post_max_size' ), ini_get( 'upload_max_filesize' ) ); ?>B.</p>

          <?php if ($this->data['id'] > 0 && $this->data['photo']->filename !== ""): ?>
              <img src="<?php echo $this->data['photo']->filename; ?>" alt="" width="203" />
          <?php endif; ?>

          <?php $this->showError('photo'); ?>
      </div>    
      <div class="form-group <?php $this->echoIfError('description', "has-error has-feedback"); ?>">
          <label for="description" class="col-sm-2 control-label">Description</label>
           <div class="col-xs-4">
            <textarea id="description" name="description" class="form-control"><?php $this->ee($this->data['description']); ?></textarea>
          </div>  
          <?php $this->showError('description'); ?>
      </div>

      <div class="form-group <?php $this->echoIfError('catalogueid', "has-error has-feedback"); ?>">
        <label for="catalogueid" class="col-sm-2 control-label">Pin in Catalogue</label>
         <div class="col-xs-4">
          <select id="catalogueid" name="catalogueid" class="form-control">
            <?php $this->showSelectOptions($this->catalogues, $this->data['catalogueid']); ?>
          </select>
      </div>
        <?php $this->showError('catalogueid'); ?>
      </div>

      <div class="btn btn-block right cf">
          <input type="hidden" name="action" value="add" />
          <button class="btn btn-primary action-btn-1">
              <span></span> Upload
          </button>
      </div>

    </div>
</form>