

<div class="fit-width masonrycontainer">
  <?php $column_count = 1; ?>
  <?php foreach($this->pins as $pin): ?>

    <?php if ($column_count === 1): ?>

    <?php endif; ?>

    <div class="item">
          <a href="./?page=singlepin&amp;id=<?php $this->ee($pin->data['id']); ?>"><img class="thumbnail-img" src="./<?php $this->ee($pin->data['photo']->filename); ?>" width="260"></a>

          <div class="pin_owner_comment">
            <p><?php $this->ee($pin->data['description']); ?></p> 
          </div>
          <div class="pin_owner">
            <img src="<?php echo $this->get_gravatar($pin->catalogue->user->data['email']); ?>" />

              <ul>
                <a href="./?page=userid&amp;id=<?php $this->ee($pin->catalogue->user->data['id']); ?>"><li><?php $this->ee($pin->catalogue->user->data['name']); ?></li></a> 
                <a href="./?page=catalogue&amp;id=<?php $this->ee($pin->catalogue->id); ?>"><li><?php $this->ee($pin->catalogue->data['name']); ?></li></a> 
              </ul> 
          </div>

          <?php foreach ($pin->comments as $comment): ?>
          <div class="pin-user-comment">
             <img src="<?php echo $this->get_gravatar($comment->user->data['email']); ?>" />
              <ul>
                <a href="./?page=userid&amp;id=<?php $this->ee($comment->user->id); ?>"><li><?php $this->ee($comment->user->data['name']); ?></li></a>
                <li><p><?php $this->ee($comment->data['comment']); ?></p></li>
              </ul> 
          </div>
          <?php endforeach; ?>

          <div class="pin-user-commentbox-add">
            <form action="./?page=comment" method="POST">
              <textarea class="comment_add_modal" name="comment" placeholder="Add a comment..."></textarea>
              <input type="hidden" name="action" value="add" />
              <input type="hidden" name="postid" value="<?php echo $pin->id; ?>" />
              <button type="submit" class="btn btn-default btn-block navbar-btn navbar-right comment_add_submit">Submit</button>
            </form>
          </div>        
    </div> 
      <?php if ($column_count === 5): ?>

      <?php endif; ?>

      <?php
        $column_count += 1;
        if ($column_count > 3) { 
          $column_count = 1;
        }
      ?>

      <?php endforeach; ?>
</div> <!-- masonry container -->



