 <form class="form-horizontal" role="form" enctype="multipart/form-data" method="POST" action="./?page=catalogue">
    <div class="container-login">      
        <div class="form-group <?php $this->echoIfError('name', "has-error has-feedback"); ?>">
            <label for="name" class="col-sm-2 control-label">Add a Catalogue</label>
            <div class="col-xs-4">
                <input id="name" name="name" class="form-control"
                value="<?php $this->ee($this->data['name']); ?>"/>
            </div>    
            <?php $this->showError('name'); ?>
        </div>

        <div class="form-group <?php $this->echoIfError('photo', "has-error has-feedback"); ?>">
            <label for="photo" class="col-sm-2 control-label">Add Photo to Catalogue</label>
            <div class="col-xs-4">
            <input id="photo" name="photo" class="form-control" type="file" />
            </div>
        </div> 
      
      <div>  
        <p class="help-block">File must be a JPG, PNG or GIF smaller than 
              <?php echo min( ini_get( 'post_max_size' ), ini_get( 'upload_max_filesize' ) ); ?>B.</p>

          <?php if ($this->data['id'] > 0 && $this->data['photo']->filename !== ""): ?>
              <img src="<?php echo $this->catalogue->data['photo']->filename; ?>" alt="" width="203" />
          <?php endif; ?>

          <?php $this->showError('photo'); ?>
      </div>    

        <div class="form-group btn btn-block right cf ">
            <input type="hidden" name="action" value="add" />
            <button class="btn btn-primary action-btn">
                <span></span> Add
            </button>
        </div>
    </div>    
</form>