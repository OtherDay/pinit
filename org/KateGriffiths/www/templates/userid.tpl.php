
<div class="fit-width masonrycontainer">

  <?php $column_count = 1; ?>
<!-- start pin loop -->
  <?php foreach($this->pins as $pin): ?>
<!-- if column count is === 1 start new row -->
    <?php if ($column_count === 1): ?>  
    <?php endif; ?>
    <div class="item">
<!-- if owner of pin is logged in -->
        <?php if ($this->user->data['id'] > 0 && $this->user->data['id'] === $pin->user->data['id']): ?>
            <div class="form-group cat-btn pin-btns">
                <form class="formbuttons" method="POST" enctype="multipart/form-data" action="./?page=pins&amp;id=<?php $this->ee($pin->data['id']); ?>">
                    <input type="hidden" name="delete" value="delete"/>
                    <button type="submit" class="btn btn-danger cat-btn"><span>Delete</span></button>
                </form>
                <form class="formbuttons" method="POST" enctype="multipart/form-data" action="./?page=newsfeed&amp;id=<?php $this->ee($pin->data['id']); ?>">
                    <div class="pin-btns-style">
                        <input type="hidden" name="add" value="add"/>
                        <button type="submit" class="btn btn-default pinit pin-btns-style"><span></span>Pin It</button>
                    </div>
                </form>
            </div>
        <?php endif; ?>    
    <!-- pin img -->
        <a href="./?page=singlepin&amp;id=<?php $this->ee($pin->data['id']); ?>"><img class="thumbnail-img" src="./<?php $this->ee($pin->data['photo']->filename); ?>" width="260"></a>
    <!-- pin description -->
        <div class="pin_owner_comment">
            <p><?php $this->ee($pin->data['description']); ?></p> 
        </div>
    <!-- pin owner -->
        <div class="pin_owner">
            <img src="<?php echo $this->get_gravatar($pin->catalogue->user->data['email']); ?>" />
            <ul>
                <a href="./?page=userid&amp;id=<?php $this->ee($pin->catalogue->user->id); ?>"><li><?php $this->ee($pin->user->data['name']); ?></li></a> 
                <a href="./?page=catalogue&amp;id=<?php $this->ee($pin->catalogue->id); ?>">
                    <li><?php $this->ee($pin->catalogue->data['name']); ?></li>
                </a> 
            </ul> 
        </div>

    <!-- comment loop -->
        <?php foreach ($pin->comments as $comment): ?>
            <div class="pin-user-comment">
                <img src="<?php echo $this->get_gravatar($comment->user->data['email']); ?>" />
                <ul>
                    <a href="./?page=userid&amp;id=<?php $this->ee($comment->user->id); ?>"><li><?php $this->ee($comment->user->data['name']); ?></li></a>
                    <li><p><?php $this->ee($comment->data['comment']); ?></p></li>
                </ul> 
            </div>
        <?php endforeach; ?>
    <!-- comment form box -->
        <div class="pin-user-commentbox-add">
            <form action="./?page=comment" method="POST">
                <textarea class="comment_add_modal" name="comment" placeholder="Add a comment..."></textarea>
                <input type="hidden" name="action" value="add" />
                <input type="hidden" name="postid" value="<?php echo $pin->id; ?>" />
                <button type="submit" class="btn btn-default btn-block navbar-btn navbar-right comment_add_submit">Submit</button>
            </form>
        </div>        
    <!-- end column -->
    </div>

    <!-- end row -->
            <?php if ($column_count === 5): ?>

            <?php endif; ?>

            <?php
                $column_count += 1;
                if ($column_count > 3) { 
                    $column_count = 1;
                }
            ?>

            <?php endforeach; ?>
</div> <!-- Container -->





