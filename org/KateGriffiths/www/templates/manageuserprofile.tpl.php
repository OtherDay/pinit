<div class="container container-accountsettings"> 
<form class="form-horizontal" role="form" enctype="multipart/form-data" method="POST" action="./?page=password" novalidate>
   
<!--ACCOUNT SETTINGS--> 
    <h3 class="accountH3">ACCOUNT SETTINGS</h3>

<!--CURRENT PASSWORD-->

        <div class="form-group">
            <label for="username" class="col-sm-2 control-label">Username</label>
            <div class="col-xs-7">
            <input name="username" type="text" class="form-control" value="<?php $this->ee($this->data['username']); ?>" id="username" placeholder="Username..." readonly>
            </div>
        </div>

        <div class="form-group <?php $this->echoIfError('oldpassword', "has-error has-feedback"); ?>">
            <label for="oldpassword" class="col-sm-2 control-label">Current Password</label>
            <div class="col-xs-7">
            <input id="oldpassword" name="oldpassword" type="password"  placeholder="Current Password..." class="form-control"/>
            </div>    
            <?php $this->showError('oldpassword'); ?>
        </div>

<!--NEW PASSWORD-->
        <div class="form-group <?php $this->echoIfError('password', "has-error has-feedback"); ?>">
            <label for="password" class="col-sm-2 control-label">New Password</label>
            <div class="col-xs-7">
                <input id="password" name="password" type="password" placeholder="New Password..." class="form-control"/>
            </div>    
            <?php $this->showError('password'); ?>
        </div>


<!--PASSWORD CONFIRM-->
        <div class="form-group <?php $this->echoIfError('password2', "has-error has-feedback"); ?>">
            <label for="password2" class="col-sm-2 control-label">Confirm Password</label>
            <div class="col-xs-7">
                <input id="password2" name="password2" type="password" placeholder="Confirm Password" class="form-control"/>
            </div>    
            <?php $this->showError('password2'); ?>
        </div>       
 <!-- EMAIL -->
         <div class="form-group">
            <label for="email" class="col-sm-2 control-label">Email Address</label>
            <div class="col-xs-7">
                <input id="email" name="email" type="email" class="form-control" placeholder="Email"
                value="<?php $this->ee($this->data['email']); ?>" />
            </div>
        </div>
        
<!--FORM BUTTON-->
        <div class="btn btn-block right cf">
            <input type="hidden" name="action" value="add" />
            <button type="submit" class="btn btn-default btn-save">Save</button>
        </div>          

</form> 

<!--END OF FORM ONE-->


<!--PROFILE-->


<!-- <form class="form-horizontal" role="form" enctype="multipart/form-data" method="POST" action="./?page=profile">
    <h3 class="profileH3">PROFILE</h3> -->
            <!-- EMAIL -->

            <!--NAME-->
   <!--      <div class="form-group">
            <label for="name" class="col-sm-2 control-label">Name</label>
            <div class="col-xs-7">
              <input name="name" type="text" class="form-control" value="<?php //$this->ee($this->data['name']); ?>" id="name" placeholder="Name...">
            </div>
         </div> -->

        <!--USERNAME-->
      <!--   <div class="form-group">
            <label for="username" class="col-sm-2 control-label">Username</label>
            <div class="col-xs-7">
              <input name="username" type="text" class="form-control" value="<?php //$this->ee($this->data['username']); ?>" id="username" placeholder="Username..." readonly>
            </div>
          </div> -->

        <!--ABOUT ME-->
       <!--  <div class="form-group">
            <label for="about me" class="col-sm-2 control-label">About me</label>
            <div class="col-xs-7">
                <textarea name="about" type="text" class="form-control" value="<?php //$this->ee($this->data['about']); ?>" id="about me" placeholder="About me..."></textarea>
            </div>
         </div>
 -->
        <!--LOCATION-->
   <!--      <div class="form-group">
            <label for="location" class="col-sm-2 control-label">Location</label>
            <div class="col-xs-7">
                <input name="location" type="text" class="form-control" value="<?php //$this->ee($this->data['location']); ?>" id="location" placeholder="Location..."/>
            </div>
         </div> -->

        <!--WEBSITE-->
     <!--    <div class="form-group">
            <label for="website" class="col-sm-2 control-label">Website</label>
            <div class="col-xs-7">
                <input name="website" type="text" class="form-control" value="<?php //$this->ee($this->data['website']); ?>" id="website" placeholder="Website..."/>
            </div>
         </div>      -->

        <!--AVATAR-->
   <!--      <div class="form-group">
            <label for="photo" class="col-sm-2 control-label">Avatar</label>
        <div class="col-xs-7">
            <input name="photo" type="file" id="photo" value="<?php //$this->ee($this->data['photo']); ?>"/>  
        <p class="help-block"> -->
  <!--       <?php //if ($this->data['id'] > 0 && $this->data['photo']->filename !== ""): ?>
            <img src="<?php //echo $this->data['photo']->filename; ?>" alt="" width="100" />
          <?php //endif; ?>

          <?php //$this->showError('photo'); ?>
      </div>     -->
                

        <!--FORM BUTTONS-->
    <!--      <div class="btn right margin">
            <input type="hidden" name="action" value="edit" />
            <button type="submit" class="btn btn-default">Save Changes</button>
         </div>


         <div class="btn right">
              <button type="submit" class="btn btn-default">Cancel</button>
         </div>      

    
</form> -->
</div>
