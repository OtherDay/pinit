<?php

require "config.inc.php";

session_start();
session_regenerate_id(true);

require "lib/http_response_code.inc.php"; 
require "lib/password.php"; 

function pinit_autoload($className) {
	require ("classes/" . $className . ".php");
}

spl_autoload_register("pinit_autoload");

// DATABASE
try {
	// connect to mysql
	$db = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
	// throw exceptions when we have sql errors
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	// do not emulate preparation of statements
	$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
} catch (PDOException $ex) {
	if (HOST_TYPE === "dev") {
		echo "<pre>";
		print_r($ex);
		echo "</pre>";
		throw $ex;
	}
}

// GET CURRENT USER
if (isset($_SESSION['user']) && isset($_SESSION['user']['id'])) {
	$usermodel = new UserModel($db, $_SESSION['user']['id']);
} else {
	$usermodel = new UserModel($db);
}

// CONTROLLER

if (isset($_GET['page'])) {
	$requestedPage = $_GET['page'];
} else {
	$requestedPage = "";
}

switch ($requestedPage) {
	


	case "":





	case "landing":
		if ($usermodel->id > 0 ) {
			header("Location: ./?page=newsfeed");
		} else {
			$page = new LandingPageView();
		}
		break;
	



	
	case "newsfeed":	
			$p = 1;
			if (isset($_GET['p'])) {
				$p = $_GET['p'];	
			}
			$limit = 20;
			$offset = ($p - 1) * $limit;
			$result = PinsModel::fetchLimit($db, $limit, $offset);
			$page = new NewsfeedPageView($result);
			break;





	case "pins": 
		$pin_id = 0;
		if (isset($_GET['id'])) {
			$pin_id = (int)$_GET['id'];
		}
		// ACTION MODE
		if (isset($_GET['action']) && $_GET['action'] === "add") {
			$mode = "blank"; // load blank form
		} else if (isset($_POST['action']) && $_POST['action'] === "add") {
			$mode = "create"; // validate and insert
		} else if (isset($_POST['delete'])) {
			$mode = "delete";
		} else if (isset($_GET['action']) && $_GET['action'] === "edit") {
			$mode = "edit"; // load unsubmitted form
		} else if (isset($_POST['action']) && $_POST['action'] === "edit") {
			$mode = "update"; // validate and update
		} else {
			$mode = "view"; // 
		}
		

		// MODEL
		if ($mode === "create") {

			$photo = new Image;
			$photo->importUpload('photo', "img/pins/", null, true );
			//var_dump($photo);
			$data = array_merge($_POST, array('photo' => $photo));

			$model = new PinsModel($db, $data);
			if ($model->id > 0) {
				// insert succeeded, redirect!
				header("Location: ./?page=newsfeed&id=" . $model->id);
				exit;
			}
		} else if ($mode === "update") {
			$model = new PinsModel($db, $pin_id);
			$model->processInput($_POST);
			if ($model->validateData()) {
				// validation success, commit and redirect;
				$model->commit();
				header("Location: ./?page=newsfeed&id=" . $model->id);
				exit;
			}
		} else if ($mode === "delete") {
			$model = new PinsModel($db, $pin_id);
			$model->remove();
			header("Location: ./?page=newsfeed");
			exit;
		} else if ($mode === "blank" || $mode === "view" || $mode === "edit") {
			$model = new PinsModel($db, $pin_id);
		}
		
		// VIEW
		if ($mode === "create" || $mode === "blank" || $mode === "edit" || $mode === "update") {
			$catalogues = CatalogueModel::fetchAllByUser($db, $usermodel->id);
			$page = new ManagePinFormView($model, $catalogues);
		} else if ($mode === "view") {
			if (isset($model->data['id']) && $model->data['id'] > 0) {
				$page = new SinglePinPageView($model);
			} else {
				// couldn't find the the pin
				$page = new NotFoundPageView();
			}
		}
		break;






	case "singlepin": 
		$singlepin_id = 0;
		if (isset($_GET['id'])) {
			$singlepin_id = $_GET['id'];	
		}
		$model = new PinsModel($db, $singlepin_id);
		//echo "<pre>"; print_r($model); echo "</pre>";	
		$page = new SinglePinPageView($model);
		break;





	case "userid":
		$user_id = 0;
		if (isset($_GET['id'])) {
			$user_id = $_GET['id'];	
		}
		$collection = PinsModel::fetchAllByUserId($db, $user_id); // call the factory
		// echo "<pre>"; print_r($collection); echo "</pre>";
		$page = new UserIdPageView($collection);
		break;






	case "catalogues":
		$p = 1;
		if (isset($_GET['p'])) {
			$p = $_GET['p'];	
		}
		$limit = 20;
		$offset = ($p - 1) * $limit;
		$result = CatalogueModel::fetchLimit($db, $limit, $offset);
		$page = new CataloguesPageView($result);
		break;


	


	case "catalogue":
		$cat_id = 0;
		if (isset($_GET['id'])) {
			$cat_id = (int)$_GET['id'];
		}
		// ACTION MODE
		if (isset($_GET['action']) && $_GET['action'] === "add") {
			$mode = "blank"; // load blank form
		} else if (isset($_POST['action']) && $_POST['action'] === "add") {
			$mode = "create"; // validate and insert
		} else if (isset($_POST['delete'])) {
			$mode = "delete";
		} else if (isset($_GET['action']) && $_GET['action'] === "edit") {
			$mode = "edit"; // load unsubmitted form
		} else if (isset($_POST['action']) && $_POST['action'] === "edit") {
			$mode = "update"; // validate and update
		} else {
			$mode = "view"; // show pin page
		}


		// MODEL
		if ($mode === "create") {

			$photo = new Image;
			$photo->importUpload('photo', "img/catalogue/", null, true );
			$data = array_merge($_POST, array('photo' => $photo, 'userid' => $usermodel->id));

			$model = new CatalogueModel($db, $data);
			if ($model->id > 0) {
				// insert succeeded, redirect!
				header("Location: ./?page=newsfeed&id=" . $model->id);
				exit;
			}
		} else if ($mode === "update") {
			$photo = new Image;

			if (isset($_FILES['photo']) && $_FILES['photo']['size'] > 0) {
				$photo->importUpload('photo', "img/catalogue/", null, true );
				$data = array_merge($_POST, array('photo' => $photo));
			} else {
				$data = $_POST;
			}			

			$model = new CatalogueModel($db, $cat_id);
			$model->processInput($data);
			if ($model->validateData()) {
				// validation success, commit and redirect;
				$model->commit();
				header("Location: ./?page=catalogue&id=" . $model->id);
				exit;
			}
		} else if ($mode === "delete") {
			$model = new CatalogueModel($db, $cat_id);
			$model->remove();
			header("Location: ./?page=catalogue");
			exit;
		} else if ($mode === "blank" || $mode === "view" || $mode === "edit") {
			$model = new CatalogueModel($db, $cat_id);
		}
		
		// VIEW
		if ($mode === "create" || $mode === "blank" || $mode === "edit" || $mode === "update") {
			$page = new ManageCatalogueFormView($model);
		} else if ($mode === "view") {
			if (isset($model->data['id']) && $model->data['id'] > 0) {
				$pins = $model->getPins();
				$page = new CataloguePageView($model, $pins);
			} else {
				// couldn't find the single pin page
				$page = new NotFoundPageView();
			}
		}
		break;





	case "comment":
		$comment = 0;
		if (isset($_GET['id'])) {
			$comment = (int)$_GET['id'];
		}
		// ACTION MODE
		if (isset($_GET['action']) && $_GET['action'] === "add") {
			$mode = "blank"; // load blank form
		} else if (isset($_POST['action']) && $_POST['action'] === "add") {
			$mode = "create"; // validate and insert
		} else if (isset($_POST['delete'])) {
			$mode = "delete";
		} else if (isset($_GET['action']) && $_GET['action'] === "edit") {
			$mode = "edit"; // load unsubmitted form
		} else if (isset($_POST['action']) && $_POST['action'] === "edit") {
			$mode = "update"; // validate and update
		} else {
			$mode = "view"; 
		}


		// MODEL
		if ($mode === "create") {
			$model = new CommentModel($db, array_merge($_POST, array('userid' => $usermodel->id)));
			if ($model->id > 0) {
				// insert succeeded, redirect!
				header("Location: ./?page=newsfeed");//. $model->data['postid']);
				exit;
			}
		} else if ($mode === "update") {
			$model = new CommentModel($db, $comment);
			$model->processInput($_POST);
			if ($model->validateData()) {
				// validation success, commit and redirect;
				$model->commit();
				header("Location: ./?page=newsfeed");// . $model->id);
				exit;
			}
		} else if ($mode === "delete") {
			$model = new CommentModel($db, $comment);
			$model->remove();
			header("Location: ./?page=pins");
			exit;
		} else if ($mode === "blank" || $mode === "view" || $mode === "edit") {
			$model = new CommentModel($db, $comment);
		}
		
		// VIEW
		if ($mode === "create" || $mode === "blank" || $mode === "edit" || $mode === "update") {
			//$page = new ManageCommentFormView($model);
		} else if ($mode === "view") {
			if (isset($model->data['id']) && $model->data['id'] > 0) {
				$page = new NewsfeedPageView($model);
			} else {
				// couldn't find the pin comment view
				$page = new NotFoundPageView();
			}
		}
		break;






	case "catalogues":
		$cat_id = 0;
		if (isset($_GET['id'])) {
			$cat_id = $_GET['id'];	
		}
		$collection = CatalogueModel::fetchAll($db); // call the factory
		 echo "<pre>"; print_r($collection); echo "</pre>";
		$page = new CataloguePageView($model, $collection);
		break;






	case "search":	
		$collection = UserModel::fetchAllBySearch($db, $_GET['query']); // call the factory
		//echo "<pre>"; print_r($collection); echo "</pre>";
		$page = new SearchUsersPageView($collection, $_GET);
		break;






	case "register":
		// get a blank user
		if (isset($_POST['action']) && $_POST['action'] === "add") {
			$model = new UserModel($db, $_POST);
			if ($model->id > 0) {
				// user register success
				$_SESSION['flash'] = "User '".$model->data['username']."' successfully created. Log in.";
				header("Location: ./");
				exit;
			}
		} else {
			$model = new UserModel($db);
		}
		// get the register view
		$page = new UserRegisterFormView($model);
		break;





	case "login":
		if (isset($_POST['username'])) {
			$un = $_POST['username'];
			$pw = $_POST['password'];
			$model = UserModel::authenticateUser($db, $un, $pw);
			if ($model->id > 0) {
				$_SESSION['user'] = array(
					'id' => $model->id,
					'username' => $model->data['username']
				);
				// login success! Redirect to place.
				$_SESSION['flash'] = "Welcome back ".$model->data['username']."!";
				header("Location: ./?page=newsfeed");
				exit;
			}
			// return useful error data
			$model->data['username'] = $_POST['username'];
			$model->errors['username'] = " ";
			$model->errors['password'] = "The username or password you entered is incorrect.";
		} else {
			$model = new UserModel($db);
		}
		$page = new LoginFormView($model);
		break;






	case "logout":
		session_destroy();
		header("Location: ./?page=login");
		exit;
		break;






	case "profile":
		// get current user model
		$model = $usermodel;
		$model->errors['oldpassword'] = "";

		if (isset($_POST['email'])) {
			$result = $model->changeEmail($_POST['email']);
			if ($result) {
				$_SESSION['flash'] = "Email changed.";
			}
		}

		// page
		$page = new ManageUserProfileFormView($model);
		break;






	case "password":
		if (!isset($usermodel)) {
			$_SESSION['flash'] = "You're not logged in.";
			header("Location: ./?page=login");
			exit;
		}
		// get current user model
		$model = $usermodel;
		$model->errors['oldpassword'] = "";

		if (isset($_POST['oldpassword'])) {
			$result = $model->changePassword(
				$_POST['oldpassword'],
				$_POST['password'],
				$_POST['password2']
			);
			if ($result) {
				$_SESSION['flash'] = "Password changed.";
				header("Location: ./?page=profile");
				exit;
			}
		}

		// page
		$page = new ManageUserProfileFormView($model);
		break;

	default:
		$page = new NotFoundPageView();
}

if (isset($usermodel)) {
	$page->setUser($usermodel);
}
if (isset($_SESSION['flash'])) {
	$page->setFlash($_SESSION['flash']);
}
$page->render();

$_SESSION['flash'] = null;
