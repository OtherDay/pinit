-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 24, 2014 at 04:28 am
-- Server version: 5.5.36
-- PHP Version: 5.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pinit`
--

-- --------------------------------------------------------

--
-- Table structure for table `catalogue`
--

CREATE TABLE IF NOT EXISTS `catalogue` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `catalogue`
--

INSERT INTO `catalogue` (`id`, `userid`, `photo`, `name`) VALUES
(5, 1, 'img/catalogue/kitteh8.jpg', 'Kittehs'),
(6, 1, 'img/catalogue/kitteh16.jpg', 'More Kittehs'),
(9, 1, 'img/catalogue/kitteh19.jpg', 'Extreme Kittehs'),
(10, 1, 'img/catalogue/kitteh3.jpg', 'Kittehs in Space'),
(11, 1, 'img/catalogue/kitteh11.jpg', 'Blah Kittehs'),
(12, 1, 'img/catalogue/kitteh21.jpg', 'K Ki Kit'),
(14, 8, 'img/catalogue/Screen Shot 2014-03-05 at 10.42.49 am.png', 'Internet Haleriousness'),
(15, 3, 'img/catalogue/kitteh20.jpg', 'Anti Kitteh'),
(16, 10, 'img/catalogue/rd3.jpeg', 'All things Pony'),
(17, 11, 'img/catalogue/coffee2.jpg', 'Coffee'),
(18, 11, 'img/catalogue/sinluchanohayvictoria.jpg', 'Motivational Posters'),
(19, 11, 'img/catalogue/sinluchanohayvictoria.jpg', 'Motivational Posters'),
(20, 11, 'img/catalogue/sinluchanohayvictoria.jpg', 'Motivational Posters'),
(21, 11, 'img/catalogue/sinluchanohayvictoria.jpg', 'Motivational Posters');

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `postid` int(11) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `datecreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`id`, `userid`, `postid`, `comment`, `datecreated`) VALUES
(14, 1, 21, 'Much comment, many letters, wow', '2014-08-19 01:55:49'),
(15, 1, 21, 'enter', '2014-08-20 00:21:24'),
(16, 1, 21, 'nfklsdjklsd', '2014-08-20 00:29:50'),
(17, 1, 24, 'comment, wow, yes, yahuh', '2014-08-20 23:15:36'),
(18, 1, 21, 'dododododo', '2014-08-20 23:57:10'),
(19, 6, 21, 'i dont care jonas', '2014-08-21 04:25:28'),
(20, 10, 23, 'fdsdfdsfds', '2014-08-23 02:06:08'),
(21, 10, 30, 'dfdsfsdfsdf', '2014-08-23 02:21:30'),
(22, 10, 30, 'sdfsdfsdfsdf', '2014-08-23 02:21:34'),
(23, 10, 31, 'dsfdsfsdfdsfs', '2014-08-23 02:21:38'),
(24, 10, 27, 'sdfsdfsdfdsf', '2014-08-23 02:21:43'),
(25, 10, 31, 'dsfsdfsdfdsfsd', '2014-08-23 02:59:05'),
(26, 10, 22, 'much issues, many dots, wow', '2014-08-23 03:03:10'),
(27, 1, 29, 'dasdsadsadas', '2014-08-23 07:07:55'),
(28, 1, 33, 'dfnsd,jfklsdjf/ljkasjdkasjd', '2014-08-23 07:20:09'),
(29, 1, 37, 'je suis stupide', '2014-08-23 07:25:45'),
(30, 10, 37, 'ich bin ein mächtiger Zauberer', '2014-08-23 07:29:14'),
(31, 1, 30, 'fgyjghjjkhghjkgulg', '2014-08-24 00:54:26');

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE IF NOT EXISTS `post` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `catalogueid` int(11) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `datecreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=39 ;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `catalogueid`, `photo`, `description`, `datecreated`) VALUES
(23, 15, 'img/pins/kitteh18.jpg', 'Orderup!', '2014-08-19 02:48:47'),
(24, 15, 'img/pins/kitteh10.jpg', '#theendisnear', '2014-08-19 02:49:10'),
(26, 5, 'img/pins/kitteh15.jpg', 'dsadsadsa', '2014-08-21 04:39:53'),
(27, 16, 'img/pins/download.jpeg', '#rd', '2014-08-23 01:34:05'),
(28, 16, 'img/pins/rd3.jpeg', '#rd', '2014-08-23 01:34:32'),
(29, 16, 'img/pins/rainbowdash1.jpeg', '#rd', '2014-08-23 01:34:48'),
(30, 16, 'img/pins/rd2.jpeg', '#rd', '2014-08-23 01:44:29'),
(31, 16, 'img/pins/rd7.jpeg', '#rd', '2014-08-23 01:46:13'),
(32, 16, 'img/pins/rd4.jpeg', '#rd', '2014-08-23 01:46:24'),
(33, 16, 'img/pins/rd9.jpeg', '#rd', '2014-08-23 05:40:40'),
(34, 16, 'img/pins/rd100.jpg', 'rd', '2014-08-23 05:42:58'),
(35, 11, 'img/pins/kitteh7.jpg', 'fsdfdfdsfdsfkjkhkjkhjjh', '2014-08-23 06:02:07'),
(36, 11, 'img/pins/kitteh24.jpg', 'fdgdfgjkliukyjthrgefd', '2014-08-23 06:02:22'),
(37, 11, 'img/pins/kitteh26.jpg', '.,kjhgfdfghgf', '2014-08-23 06:02:39'),
(38, 17, 'img/pins/coffee.jpg', '#happymorningperson', '2014-08-24 02:08:33');

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE IF NOT EXISTS `profile` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `about` text NOT NULL,
  `location` varchar(50) NOT NULL,
  `website` varchar(50) NOT NULL,
  `avatar` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `photo` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`,`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`id`, `name`, `username`, `password`, `about`, `location`, `website`, `avatar`, `email`, `photo`) VALUES
(1, 'Kate Griffiths', 'kate', '$2y$10$xR5Q6v6h5Dqtbh0wt13bIOsi7PpMDmp4Uf9Qk7Y3yofxprhPLiSw6', '', '', '', '', 'kategriffiths83@hotmail.com', ''),
(2, 'Not Kate', 'Not Kate', '$2y$10$RTL5UbD62T7L8uCeTxVAwucmpZZwgqtbDQFPT9D8Ka1Zi6P9wfNFK', '', '', '', '', 'isveryimportant@thisplace.com', ''),
(3, 'Ting', 'tingtings', '$2y$10$b7B8MVXC2Ah9mBQb6JgKCeYmN4H4Z/b56L/NJwGuGd14yTF0lQlvK', '', '', '', '', 'tings@westartednothing.com', ''),
(4, 'Tiesto', 'tiesto', '$2y$10$j4wY2MM7zfQaWMy1LfWAweBGy6ceaPZPosF1UvLHl3erWWgQJKhPm', '', '', '', '', 'clublife@miami.com', ''),
(5, 'Sweede', 'swedishhousemafia', '$2y$10$sdK5k6c5UOWyI2Wb30NyBeNOlJ3Tfj/YA8sDzSdu77UJJ48ge1LSS', '', '', '', '', 'shm@savetheworld.com', ''),
(6, 'Shihad', 'shihad', '$2y$10$Bz2iMvDTlsHai6SRv4qisOOXjwcKxdeEdNcKs/KeuVhP5Jz4uL1g.', '', '', '', '', 'shihad@nz.com', ''),
(7, 'Pharrell', 'Pharrell', '$2y$10$4bJOvlFRYaNDBIcv8L01/uuoA.1KyYh.ZV1pstX5/zw..I1/ETPNC', '', '', '', '', 'williams@happy.com', ''),
(8, 'Nero', 'Nero', '$2y$10$GcmH9yrv103KITXy58jO1.JRGLIA43gWekGRDoVHy1DkfoUZRNHNW', '', '', '', '', 'nero@promises.com', ''),
(9, 'Mia', 'mia', '$2y$10$DVqtyVXunhd0v1eTP9zsye2TnTK3eG/6SwpbTm3mpCylEtgV7cxfa', '', '', '', '', 'mia@badgirl.com', ''),
(10, 'Jonas', 'Jonas', '$2y$10$acLbRoad5zd0W8zhgUFWtu1lP0lyaldfaCzWeXNQUw38bTJ.KMLqm', '', '', '', '', 'jonas.ermen@gmail.com', ''),
(11, 'Admin', 'Admin', '$2y$10$fdoXeuiuDCVGx/D0l8h9wugGMHcZsu9pjoups4h4erOd86XUAI2Ja', '', '', '', '', 'admin@admin.com', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
